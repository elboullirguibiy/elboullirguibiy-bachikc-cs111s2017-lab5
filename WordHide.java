//*************************************
// Honor Code: The work we are submitting is a result of our own thinking and efforts.
// Yosra El Boulli Rguibi - Cassie Bachik
// CMPSC 111 Spring 2017
// Lab 5
// Date: February 20, 2017
//
// Purpose: To hide a secret word. 
//*************************************
import java.util.Date;
import java.util.Scanner;

public class WordHide
{
	//-----------------------------
	//main method: program execution begins here
	//-----------------------------
	public static void main (String[] args)
	{
	  //label output with name and date:
	System.out.println("Yosra El boulli Rguibi, Cassie Bachik\nLab5\n"+ new Date()+ "\n");
	String secret, mutation1, mutation2, mutation3, mutation4, mutation5, mutation6, mutation7, mutation8, mutation9, mutation10, mutation11, mutation12;

	Scanner scan = new Scanner(System.in);
	System.out.println("Please enter a word or phrase composed of ten letters:  ");  
	secret = scan.next();

	mutation1= secret.substring (0,10);
	mutation2= mutation1.toUpperCase();


	System.out.println("The 10 uppercase characters are: " +mutation2);

	mutation3= mutation2.substring (0,1);
	mutation4= mutation2.substring (1,2);
	mutation5= mutation2.substring (2,3);
	mutation6= mutation2.substring (3,4);
	mutation7= mutation2.substring (4,5);
	mutation8= mutation2.substring (5,6);
	mutation9= mutation2.substring (6,7);
	mutation10= mutation2.substring (7,8);
	mutation11= mutation2.substring (8,9);
	mutation12= mutation2.substring (9,10);


	System.out.println("A B C Y E F G H I J K L M N O P B R S T "); 
	System.out.println("F U P W X Y Z G H I J K L M G O R Q R S ");
	System.out.println("E B E L E F G A I B H L M D O P G R S T "); 
	System.out.println("R B R D E F G S I J K L M V Y P O R " +mutation3 + " T "); 
	System.out.println("T U R J X Y Z Q H I J K L G N O P " +mutation4 + " F S "); 
	System.out.println("K U T W X Y Z I P I J K L R Y O " +mutation5 + " Q R F "); 
	System.out.println("L B J O E O G O I J K L M N O " +mutation6 + " Q R S T "); 
	System.out.println("A U P W X Y Z A H I J K L A " +mutation7 + " O W Q R S "); 
	System.out.println("Q B L M E F G B I J K L M " +mutation8 + " O P O R S T "); 
	System.out.println("U U F W X Y Z H H I J K " +mutation9 + " B N O P Q R S "); 
	System.out.println("T A Y E E F G P I J K " +mutation10 + " M D R P G R S T "); 
	System.out.println("S H D W X Y Z T H I " +mutation11 + " K L L N O W Q R S "); 
	System.out.println("Y G C R E F G D I " +mutation12 + " K L M Y O P Y R S T "); 
	System.out.println("O J B W X Y Z R H R J K L E R O T Q R S "); 
	System.out.println("P H C D E F G R I E W L M T I P Q G S T "); 
	System.out.println("L I H Y X Y Z R H I J K L Y N P Q H S T "); 
	System.out.println("W P C D E F G D I J R S D L O P H R S T "); 
	System.out.println("E R P Y X Y Z T H I J K L E F O V Q R S "); 
	System.out.println("R I C T E F G T I J K L M U O L D R S T "); 
	System.out.println("T W V J X Y Z O H I J K L F G O S Q R S "); 
	


	}
}
